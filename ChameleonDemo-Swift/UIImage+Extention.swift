//
//  UIImage+Extention.swift
//  ChameleonDemo-Swift
//
//  Created by Aina on 24/07/17.
//  Copyright © 2017 Vicc Alexander. All rights reserved.
//

import UIKit

extension UIImage {
    
    /// Returns a image that fills in newSize
    func resizedImage(newSize: CGSize) -> UIImage {
        // Guard newSize is different
        guard self.size != newSize else { return self }
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        self.draw(in:CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func crop( rect: CGRect) -> UIImage {
        var rect = rect
        rect.origin.x*=self.scale
        rect.origin.y*=self.scale
        rect.size.width*=self.scale
        rect.size.height*=self.scale
        var image = UIImage()
        if let imageRef = self.cgImage!.cropping(to: rect)
        {
            image = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)
            
        }
        return image
    }
}

extension UIColor {
        var coreImageColor: CIColor {
            return CIColor(color: self)
        }
        var components: (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
            let coreImageColor = self.coreImageColor
            return (coreImageColor.red, coreImageColor.green, coreImageColor.blue, coreImageColor.alpha)
        }
}

