//
//  ImageViewController.swift
//  ChameleonDemo-Swift
//
//  Created by Aina on 13/07/17.
//  Copyright © 2017 Vicc Alexander. All rights reserved.
//

import UIKit
import  Chameleon
import AVFoundation
import Photos

class ImageViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIGestureRecognizerDelegate
{
    var image : UIImage?
    var picker : UIImagePickerController?
    var gesture : UITapGestureRecognizer?
    var selectedViewForImageOne : UIView?
    var selectedViewForImageTwo : UIView?
    var selectedViewForImageThree : UIView?
    var selectedViewForImageFour : UIView?
    var tag = 0
    
    //MARK: - Application Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedViewForImageOne = UIView(frame: CGRect.zero)
        selectedViewForImageTwo = UIView(frame: CGRect.zero)
        selectedViewForImageThree = UIView(frame: CGRect.zero)
        selectedViewForImageFour = UIView(frame: CGRect.zero)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - IBAction Methods
    
    /// Take photo action
    ///
    /// - Parameter sender: sender as Any
    @IBAction func didTapTakePhoto(_ sender: Any) {
        picker = UIImagePickerController()
        picker?.delegate = self
        tag = (((sender as? UIButton)?.superview)?.tag)!
        openCamera()
    }
    
    /// Load default images action
    ///
    /// - Parameter sender: sender as Any
    @IBAction func didTapLoadDefaultImages(_ sender: Any) {
        //load default images
        tag = (((sender as? UIButton)?.superview)?.tag)!
        switch (sender as? UIButton)!.tag
        {
        case 111:
            if let image = UIImage(named: "tooth1New.jpeg")
            {
                DispatchQueue.main.async {
                    self.setImage(img: image)
                }
            }
        case 112:
            if let image = UIImage(named: "tooth2.jpeg")
            {
                DispatchQueue.main.async {
                    self.setImage(img: image)
                }
            }
        case 113:
            if let image = UIImage(named: "tooth3.jpeg")
            {
                DispatchQueue.main.async {
                    self.setImage(img: image)
                }
            }
        case 114:
            if let image = UIImage(named: "tooth4.jpeg")
            {
                DispatchQueue.main.async {
                    self.setImage(img: image)
                }
            }
        default:
            break
        }
    }
    
    /// Close image view action
    ///
    /// - Parameter sender: sender as Any
    @IBAction func didTapCloseImageButton(_ sender: Any) {
        if let btntag = (sender as? UIButton)?.tag
        {
            switch btntag {
            case 101:
                self.view.viewWithTag(100)?.isHidden = false
            case 102:
                self.view.viewWithTag(300)?.isHidden = false
            case 103:
                self.view.viewWithTag(200)?.isHidden = false
            case 104:
                self.view.viewWithTag(400)?.isHidden = false
            default:
                break
            }
        }
    }
    
    /// Chose from gallery action
    ///
    /// - Parameter sender: sender as Any
    @IBAction func didTapChooseFromGallery(_ sender: Any) {
        picker = UIImagePickerController()
        picker?.delegate = self
        tag = (((sender as? UIButton)?.superview)?.tag)!
        openGallery()
    }
    
    //MARK: -  UIImagePickerController delegate methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        picker .dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            DispatchQueue.main.async {
                self.setImage(img: image)
            }
        }
    }
    
    func imagePickerControllerDidCancel( _ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: -  Private methods
    
    /// Set image for the image view
    ///
    /// - Parameter img: image passed
    func setImage(img : UIImage)
    {
        switch self.tag
        {
        case 100:
            if let imgView = self.view.viewWithTag(10) as? UIImageView
            {
                let resizedImage = img.resizedImage(newSize: imgView.frame.size)
                imgView.image = resizedImage
                self.view.viewWithTag(self.tag)?.isHidden = true
                addGesture(imgview: imgView)
            }
        case 200:
            if let imgView = self.view.viewWithTag(20) as? UIImageView
            {
                let resizedImage = img.resizedImage(newSize: imgView.frame.size)
                imgView.image = resizedImage
                self.view.viewWithTag(self.tag)?.isHidden = true
                addGesture(imgview: imgView)
            }
        case 300:
            if let imgView = self.view.viewWithTag(30) as? UIImageView
            {
                let resizedImage = img.resizedImage(newSize: imgView.frame.size)
                imgView.image = resizedImage
                self.view.viewWithTag(self.tag)?.isHidden = true
                addGesture(imgview: imgView)
            }
        case 400:
            if let imgView = self.view.viewWithTag(40) as? UIImageView
            {
                let resizedImage = img.resizedImage(newSize: imgView.frame.size)
                imgView.image = resizedImage
                self.view.viewWithTag(self.tag)?.isHidden = true
                addGesture(imgview: imgView)
            }
        default:
            break
        }
    }
    
    /// Open camera to capture image
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            picker!.sourceType = UIImagePickerControllerSourceType.camera
            let cameraMediaType = AVMediaTypeVideo
            let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: cameraMediaType)
            switch cameraAuthorizationStatus
            {
            case .authorized:
                self .present(picker!, animated: true, completion: nil)
                break
            default:
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                    if granted == true
                    {
                        // Permission granted
                        self .present(self.picker!, animated: true, completion: nil)
                    }
                    else
                    {
                        let titleString =  NSLocalizedString("Warning", comment: "")
                        let messageString = NSLocalizedString("NoCamera_Access_Warning", comment: "")
                        let alert = UIAlertController(title: titleString, message: messageString, preferredStyle: .alert)
                        let firstAction = UIAlertAction(title:"OK", style: .default)
                        {
                            UIAlertAction in
                        }
                        alert.addAction(firstAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                })
                break
            }
        }
        else
        {
            openGallery()
        }
    }
    
    /// Open gallery to load existing image
    func openGallery()
    {
        if PHPhotoLibrary.authorizationStatus() != .authorized
        {
            PHPhotoLibrary.requestAuthorization({
                (newStatus) in
                if newStatus ==  PHAuthorizationStatus.authorized
                {
                    self.picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
                    if UIDevice.current.userInterfaceIdiom == .phone
                    {
                        self.present(self.picker!, animated: true, completion: nil)
                    }
                    else
                    {
                        self.picker?.modalPresentationStyle  = .popover
                        if let popOver  = self.picker?.popoverPresentationController
                        {
                            popOver.permittedArrowDirections    = .any
                            popOver.sourceRect                  = (self.view.viewWithTag(self.tag)?.bounds)!
                            popOver.sourceView                  = self.view.viewWithTag(self.tag)
                            self.present(self.picker!, animated: true, completion: nil)
                        }
                    }
                }
                else
                {
                    let titleString = NSLocalizedString("Warning", comment: "")
                    let messageString = NSLocalizedString("NoPhoto_Access_Warning", comment: "")
                    let alert = UIAlertController(title: titleString, message: messageString, preferredStyle: .alert)
                    let firstAction = UIAlertAction(title:"OK", style: .default)
                    {
                        UIAlertAction in
                    }
                    alert.addAction(firstAction)
                    self.present(alert, animated: true, completion: nil)
                    return
                }
            })
        }
        else
        {
            self.picker!.sourceType = UIImagePickerControllerSourceType.photoLibrary
            if UIDevice.current.userInterfaceIdiom == .phone
            {
                self.present(self.picker!, animated: true, completion: nil)
            }
            else
            {
                self.picker?.modalPresentationStyle  = .popover
                if let popOver  = self.picker?.popoverPresentationController
                {
                    popOver.permittedArrowDirections    = .any
                    popOver.sourceRect                  = (self.view.viewWithTag(tag)?.bounds)!
                    popOver.sourceView                  = self.view.viewWithTag(tag)
                    self.present(self.picker!, animated: true, completion: nil)
                }
            }
        }
    }
    
    /// Add gesture to the image view
    ///
    /// - Parameter imgview: UIImageView
    func addGesture(imgview: UIImageView)
    {
        gesture  = UITapGestureRecognizer(target: self, action: #selector(ImageViewController.handleTap(tapper:)))
        gesture?.delegate = self
        gesture?.numberOfTapsRequired = 1
        imgview.addGestureRecognizer(gesture!)
        imgview.isUserInteractionEnabled = true
    }
    
    /// Gesture handler method
    ///
    /// - Parameter tapper: UITapGestureRecognizer
    func handleTap(tapper : UITapGestureRecognizer)
    {
        
        if let imageView = tapper.view as? UIImageView
        {
            if let img = imageView.image
            {
                let avgColor = initChameleon(img: img)
                print("Average color for \(String(describing: imageView.tag)) imageView = \(avgColor)")
                
                //get color of desired area
                let touchPoint = tapper.location(in: imageView)
                let center = CGPoint(x: touchPoint.x, y: touchPoint.y)
                let size = CGSize(width: 50, height: 50)
                let origin = CGPoint(x: center.x - size.width/2, y:  center.y - size.height/2)
                let subImage = img.crop(rect: CGRect(origin: origin, size: size))
                let subAvgColor = initChameleon(img: subImage)
                print("Average color for subimage imageView = \(subAvgColor)")
                
                //to add border to selected area
                func addBorderToSelectedImageView(withView selectedView: UIView)
                {
                    selectedView.frame = CGRect(origin: origin, size: size)
                    selectedView.layer.cornerRadius = 10
                    selectedView.backgroundColor = UIColor.clear
                    selectedView.layer.borderWidth = 2
                    selectedView.layer.borderColor = UIColor.black.cgColor
                    imageView.addSubview(selectedView)
                }
                
                func setColorAndCodeToView(selectedView : UIView, color:UIColor)
                {
                    let colorComponents = color.components                    
                    for view in selectedView.subviews
                    {
                        if let vw =  view as? UILabel
                        {
                            switch vw.tag
                            {
                            case 1:
                                    vw.text = "R-\(String(format: "%.2f",colorComponents.red))"
                            case 2:
                                vw.text = "G-\(String(format: "%.2f",colorComponents.green))"
                            case 3:
                                vw.text = "B-\(String(format: "%.2f",colorComponents.green))"
                            default:
                                break
                            }
                        }
                        else
                        {
                        view.backgroundColor = color
                        }
                    }
                }
                
                //set average color to corresponding views
                switch imageView.tag
                {
                case 10:
                    let viewToSetColor = self.view.viewWithTag(11)
                    setColorAndCodeToView(selectedView: viewToSetColor!, color: subAvgColor)
//                    viewToSetColor?.backgroundColor = subAvgColor
                    if let view = selectedViewForImageOne
                    {
                        addBorderToSelectedImageView(withView: view)
                    }
                    
                case 20:
                    let viewToSetColor = self.view.viewWithTag(12)
                    setColorAndCodeToView(selectedView: viewToSetColor!, color: subAvgColor)
//                    viewToSetColor?.backgroundColor = subAvgColor
                    if let view = selectedViewForImageTwo
                    {
                        addBorderToSelectedImageView(withView: view)
                    }
                    
                case 30:
                    let viewToSetColor = self.view.viewWithTag(13)
                    setColorAndCodeToView(selectedView: viewToSetColor!, color: subAvgColor)
//                    viewToSetColor?.backgroundColor = subAvgColor
                    if let view = selectedViewForImageThree
                    {
                        addBorderToSelectedImageView(withView: view)
                    }
                    
                case 40:
                    let viewToSetColor = self.view.viewWithTag(14)
                    setColorAndCodeToView(selectedView: viewToSetColor!, color: subAvgColor)
//                    viewToSetColor?.backgroundColor = subAvgColor
                    if let view = selectedViewForImageFour
                    {
                        addBorderToSelectedImageView(withView: view)
                    }
                    
                default:
                    break
                }
            }
        }
    }
    
    //MARK: -  Chameleon methods
    func initChameleon(img : UIImage) -> UIColor {
        let sampleOneImageAverageColor = AverageColorFromImage(img)
        return sampleOneImageAverageColor
    }
    
}

